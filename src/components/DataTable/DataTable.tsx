import React, { useState, useEffect } from "react";
import './DataTable.css'

interface Column {
  key: string;
  label: string;
  sortable?: boolean; // Ajout de la propriété sortable par colonne
  filterable?: boolean;
  render?: (value: any) => React.ReactNode;
}

interface ApiPaginationParams {
  pageSize: number;
  ApiHasPaginator: boolean;
  paginate: boolean;
}

interface DataTableProps {
  data: any[]; // Données à afficher dans le tableau
  columns: Column[]; // Informations sur les colonnes
  sortable?: boolean; // Ajout de la prop sortable globale
  filterable?: boolean; 
  apiUrl?: string; // URL de l'API
  withPagination? : boolean,
  pageSize? : number,
  ApiPaginationParams?: ApiPaginationParams,
  ApiFilter?: boolean,
  ApiSorting?: boolean,
  ApiSearching? : boolean,
}

const DataTable: React.FC<DataTableProps> = ({ data : initialData, columns, sortable, filterable, apiUrl, 
  ApiPaginationParams = {
    pageSize : 1,
    ApiHasPaginator : false,
    paginate : false
  },
  ApiFilter = false, ApiSorting = false, ApiSearching = false
 }) => {
  const [sortedColumn, setSortedColumn] = useState<string | null>(null);
  const [sortOrder, setSortOrder] = useState<"asc" | "desc">("asc");
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [filters, setFilters] = useState<Record<string, string>>({});
  const [data, setData] = useState<any[]>(initialData);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [pageSize, setPageSize] = useState(ApiPaginationParams.pageSize);
  const [ApiHasPaginator, setApiHasPaginator] = useState(ApiPaginationParams.ApiHasPaginator);
  const [paginate, setPaginate] = useState(ApiPaginationParams.paginate);


  useEffect(() => {
    const fetchDataAndPaginate = async () => {
      if (apiUrl) {
        if(paginate){
          const { data: fetchedData, totalPages: fetchedTotalPages } = await fetchData(apiUrl, currentPage, pageSize, ApiHasPaginator);
          setData(fetchedData);
          setTotalPages(fetchedTotalPages);
        }else{
          const { data: fetchedData, totalPages: fetchedTotalPages } = await fetchData(apiUrl, currentPage, pageSize, ApiHasPaginator);
          setData(fetchedData);
          setTotalPages(fetchedTotalPages);
        }
        const { data: fetchedData, totalPages: fetchedTotalPages } = await fetchData(apiUrl, currentPage, pageSize, ApiHasPaginator);
        setData(fetchedData);
        setTotalPages(fetchedTotalPages);
      } else {
        if(paginate){
          const paginatedData = paginateData(initialData, currentPage, pageSize);
          setData(paginatedData);
          setTotalPages(Math.ceil(initialData.length / pageSize));
        }else{
          setData(initialData);
          setTotalPages(1);
        }
      }
    };
  
    fetchDataAndPaginate();
  }, [apiUrl, currentPage, initialData, pageSize, filters, sortedColumn]);

  const fetchData = async (apiUrl: string, currentPage: number, pageSize: number, ApiHasPaginator : boolean) => {
    try {
      let filterParams = "";
        for (const [key, value] of Object.entries(filters)) {
        if(value) filterParams += `&${key}=${encodeURIComponent(value)}`;
      }
      let sortParams = "";
        if (sortedColumn && sortOrder) {
          sortParams = `&order[${sortedColumn}]=${sortOrder}`;
      }
      let searchParam = "";
      if (searchTerm) {
        searchParam = `&q=${encodeURIComponent(searchTerm)}`;
      }
      const url = `${apiUrl}?page=${currentPage}&pageSize=${pageSize}`
      const fetchString = (ApiHasPaginator ? `${url}&itemsPerPage=${pageSize}` : url) + `${filterParams}${sortParams}${searchParam}`  
      const response = await fetch(fetchString);
      const jsonData = await response.json();
      return {
        data: jsonData,
        totalPages: Math.ceil(jsonData.length / pageSize),
      };
    } catch (error) {
      console.error("Error fetching data:", error);
      return {
        data: [],
        totalPages: 1,
      };
    }
  };

  const paginateData = (data: any[], currentPage: number, pageSize: number) => {
    const startIndex = (currentPage - 1) * pageSize;
    const endIndex = startIndex + pageSize;
    return data.slice(startIndex, endIndex);
  };

  const handlePrevPage = () => {
    setCurrentPage((prevPage) => Math.max(prevPage - 1, 1));
  };

  const handleNextPage = () => {
    setCurrentPage((prevPage) => Math.min(prevPage + 1, totalPages));
  };

  const handleSort = (key: string, sortableColumn: boolean | undefined) => {
    if (sortableColumn || sortable) {
      // Vérifiez si le tri est autorisé pour cette colonne ou global
      if (sortedColumn === key) {
        setSortOrder(sortOrder === "asc" ? "desc" : "asc");
      } else {
        setSortedColumn(key);
        setSortOrder("asc");
      }
    }
  };

  const handleSearch = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const handleFilterChange = (key: string, value: string) => {
    setFilters((prevFilters) => ({ ...prevFilters, [key]: value }));
  };

  const afterResearchData = ApiSearching ? data : data.filter((row) =>
    // Filtrage par recherche
    Object.values(row).some((value) => {
      return String(value).toLowerCase().includes(searchTerm.toLowerCase());
    }));

  const filteredData = ApiFilter ? afterResearchData :  afterResearchData.filter((row) =>
    // Filtrage par filtres
     Object.entries(filters).every(([key, value]) => {
      const rowValue = row[key];
      return String(rowValue).toLowerCase().includes(value.toLowerCase());
    })
  );

  const sortedData = ApiSorting ? filteredData : sortedColumn
  ? filteredData.slice().sort((a, b) => {
      const aValue = a[sortedColumn];
      const bValue = b[sortedColumn];
      if (aValue === bValue) return 0;
      return sortOrder === "asc"
        ? aValue < bValue
          ? -1
          : 1
        : aValue > bValue
        ? -1
        : 1;
    })
  : filteredData;

  



  return (
    <div className="overflow-x-auto">
      {/* Champ de recherche */}
      <div className="py-3 px-4">
          <div className="relative max-w-xs">
            <label className="sr-only">Search</label>
            <input type="text" 
              className="py-2 px-3 ps-9 block w-full border-gray-900 shadow-gray-700 shadow-sm rounded-lg text-sm focus:z-10 focus:border-blue-500 focus:ring-blue-500 disabled:opacity-50 disabled:pointer-events-none dark:bg-neutral-900 dark:border-neutral-700 dark:text-neutral-400 dark:placeholder-neutral-500 dark:focus:ring-neutral-600" 
              placeholder="Rechercher..." 
              value={searchTerm}
              onChange={handleSearch}
              />
            <div className="absolute inset-y-0 start-0 flex items-center pointer-events-none ps-3">
              <svg className="size-4 text-gray-400 dark:text-neutral-500" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                <circle cx="11" cy="11" r="8"></circle>
                <path d="m21 21-4.3-4.3"></path>
              </svg>
            </div>
          </div>
        </div>
      {/* <input
        type="text"
        placeholder="Rechercher..."
        value={searchTerm}
        onChange={handleSearch}
        className="border border-gray-500 rounded px-3 py-1 mb-4"
      />       */}
      <div className="border rounded-lg overflow-hidden dark:border-neutral-700 border-gray-900">
        <table className="min-w-full divide-y divide-gray-200 dark:divide-neutral-700">
        <thead className="px-6 py-3 text-center text-xs font-bold text-gray-500 uppercase dark:text-neutral-500">
          <tr className="">
            {columns.map((column) => (
              <th
                key={column.key}
                className="p-2 whitespace-nowrap"
              >
                <a onClick={() => handleSort(column.key, column.sortable)} 
                  className={column.sortable || sortable ? " flex flex-col cursor-pointer" : ""}
                >
                  <div>
                  <span className="font-semibold text-left">{column.label}</span>
                  { (column.sortable || sortable) &&
                    sortedColumn === column.key && (
                      <span className="text-gray-900">{sortOrder === "asc" ? " ↑" : " ↓"}</span>
                    )}
                  </div>
                  </a>
                  <input
                    type="text"
                    value={filters[column.key] || ''}
                    onChange={(e) => handleFilterChange(column.key, e.target.value)}
                    className="border border-gray-300 rounded px-3 py-1 mt-2 w-full"
                    placeholder={`Filter by ${column.label.toLowerCase()}...`}
                  />
              </th>
            ))}
          </tr>
        </thead>
        <tbody className="divide-y divide-gray-200 dark:divide-neutral-700">
          {sortedData.map((row, rowIndex) => (
            <tr key={rowIndex}>
              {columns.map((column) => (
                <td key={column.key} className="p-2 whitespace-nowrap">
                  <span className="font-medium text-gray-800">{column.render ? column.render(row[column.key]) : row[column.key]}</span>
                  </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table></div>
      { totalPages > 1 && (
        <div className="py-1 px-4">
            <nav className="flex items-center space-x-1">
              <button type="button" 
                className="p-2.5 min-w-[40px] inline-flex justify-center items-center gap-x-2 text-sm rounded-full text-gray-800 hover:bg-gray-100 disabled:opacity-50 disabled:pointer-events-none dark:text-white dark:hover:bg-white/10"
                onClick={handlePrevPage} disabled={currentPage === 1}
                >
                <span aria-hidden="true">«</span>
                <span className="sr-only">Previous</span>
              </button>
              <button type="button" className="min-w-[40px] flex justify-center items-center text-gray-800 hover:bg-gray-100 py-2.5 text-sm rounded-full disabled:opacity-50 disabled:pointer-events-none dark:text-white dark:hover:bg-white/10" 
              aria-current="page">
                {currentPage} / {totalPages}
                </button>
              <button type="button" className="p-2.5 min-w-[40px] inline-flex justify-center items-center gap-x-2 text-sm rounded-full text-gray-800 hover:bg-gray-100 disabled:opacity-50 disabled:pointer-events-none dark:text-white dark:hover:bg-white/10"
                onClick={handleNextPage} disabled={currentPage === totalPages}
              >
                <span className="sr-only">Next</span>
                <span aria-hidden="true">»</span>
                </button>
            </nav>
          </div>
          )}
    </div>
  );
};

export default DataTable;
